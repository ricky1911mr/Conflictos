/*Lorem ipsum dolor sit amet, consectetur 
adipiscing elit. Fusce eget nisi non dolor porttitor 
eleifend in non nisl. Nam tincidunt dignissim eros, sit amet
 viverra risus finibus id. Nulla blandit feugiat ultrices. Sed nec dapibus qu
 am. Fusce porttitor erat viverra lacus posuere elementum. Nunc consequat ut dolo
 r eget accumsan. Maecenas nunc elit, scelerisque sit amet leo ut, eleifend bibendum mauris.
  Vestibulum cursus aliquam orci sodales consectetur. Class aptent taciti sociosqu ad litora tor
  quent per conubia nostra, per inceptos himenaeos. Praesent sed risus at purus venenatis aliquet. Done
  c sed lacus fermentum, vestibulum est vel,
 hendrerit ligula. Praesent aliquet tristique elementum. Quisque non imperdiet quam, elementum sollicitudin nunc.*/
var b="Revision de conflictos"
async crear(UsuarioDto: UsuarioDto): Promise<Usuario> {
    const crearUsuario = new this.usuarioModel(UsuarioDto);
    return await crearUsuario.save();
  }

  async editar(id: string): Promise<Usuario> {
    return await this.usuarioModel.update();
  }

  async buscarTodos(): Promise<Usuario[]> {
    return await this.usuarioModel.find().exec();
  }