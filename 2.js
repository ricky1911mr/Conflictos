var d= "Conflictos" 
var x= 3


async crear(UsuarioDto: UsuarioDto): Promise<Usuario> {
    const crearUsuario = new this.usuarioModel(UsuarioDto);
    return await crearUsuario.save();
  }

  async editar(id: string): Promise<Usuario> {
    return await this.usuarioModel.update();
  }

  async buscarTodos(): Promise<Usuario[]> {
    return await this.usuarioModel.find().exec();
  }